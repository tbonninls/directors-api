/*
* This module contains server routes setup
* Server runs this file on initial booting
*
* Check server.js to find the place where exactly this module is called
*
* @author Ken Huh <ken123777@gmail.com>
*/

var creationController = require('../app/controllers/creationController');
var updateController = require('../app/controllers/updateController');
var listingController = require('../app/controllers/listingController');
var headerValidation = require('./middlewares/headerValidation');
var requestBody = require('./middlewares/requestBody');
var requestParameters = require('./middlewares/requestParameters');

module.exports = function(app){
  // Directors listing
  // Returns all directors in the database
  app.get('/directors', listingController.index);

  // Reset favorite_camera and favorite_movies to new values
  // At least one attribute is needed to perform update
  // favorite_camera: takes string type value
  // favorite_movies: takes an array
  // Returns the director with updated values
  app.put('/directors/:livestream_id', headerValidation.validateJSON, requestParameters.checkLivestreamId, headerValidation.authorizeMD5, updateController.reset);

  // Add a new movie to favorite_movies array
  // Only takes one movie at a time
  // favorite_movies: takes a string type value
  // Returns a successful message if updated
  app.post('/directors/:livestream_id/favorite_movies', headerValidation.validateJSON, requestParameters.checkLivestreamId, headerValidation.authorizeMD5, updateController.addMovie);

  // Director registration
  // Communicate with livestream's accouts API to fetch full_name and dob
  // Does not allow duplicate livestream_id
  // Returns the director who just joined
  app.post('/directors', headerValidation.validateJSON, requestBody.checkLivestreamId, creationController.register);
};
