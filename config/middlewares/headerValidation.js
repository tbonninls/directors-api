/*
* Handles request authorization
* Router first calls this middleware when anyone attempts to modify any information
*
* Check config/routes.js to find the place where exactly this middleware is called
*
* @author Ken Huh <ken123777@gmail.com>
*/

var mongoose = require('mongoose');
var Director = mongoose.model('Director');
var md5 = require('MD5');

// check request header for authorization
exports.authorizeMD5 = function(req, res, next){
  var livestreamId = req.params.livestream_id;

  Director.findOne({ livestream_id: livestreamId }, function(err, director){
    if (!director) {
      res.send(404, { 'Error': 'Director not found' });
    } else if (req.headers['authorization'] === "Bearer " + md5(director.full_name)) {
      next();
    } else {
      res.send(401, { 'Error': 'Authorization failed' });
    }
  });
};

// validate JSON request object
exports.validateJSON = function(req, res, next){
  if (req.headers['content-type'] !== 'application/json') {
    res.send(400, { 'Error' : 'Request must be made with JSON object' });
  } else {
    next();
  }
};
