/*
* Handles request params verification for necessary fields
* Router calls this middleware to see if there is any missing fields in the request params
*
* Check config/routes.js to find the place where exactly this middleware is called
*
* @author Ken Huh <ken123777@gmail.com>
*/

// check for livestream_id in the request params
exports.checkLivestreamId = function(req, res, next){
  var livestreamId = req.params.livestream_id;
  if (!livestreamId) {
    res.send(400, { 'Error': 'Missing Livestream ID in parameter' });
  } else {
    next();
  }
};
