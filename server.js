/*
* Main server file
* Runs all the dependencies and configuration files
*
* @author Ken Huh <ken123777@gmail.com>
*/

var express = require('express');
var http = require('http');
var path = require('path');
var fs = require('fs');
var mongoose = require('mongoose');

var app = express();

// environment variable setup
var env = process.env.NODE_ENV || 'development';

// credentials
var credentials = require('./config/credentials')(env);

// database models
var models_path = __dirname + '/app/models'
fs.readdirSync(models_path).forEach(function(file){
  if (~file.indexOf('.js')) require(models_path + '/' + file);
});

// database connection
mongoose.connect(credentials.db);

// app configuration
require('./config/environments')(app);

// app routes
require('./config/routes')(app);

// development only
if (app.get('env') === 'development') {
  app.use(express.errorHandler());
}

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
