/*
* Database configuration for Directors model
*
* @author Ken Huh <ken123777@gmail.com>
*/

var findOrCreate = require('mongoose-findorcreate');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var directorSchema = new Schema({
  livestream_id: Number,
  full_name: String,
  dob: Date,
  favorite_camera: String,
  favorite_movies: []
});

directorSchema.plugin(findOrCreate);
mongoose.model('Director', directorSchema);
